# DPS Frontend Code Activity

This code activity is an assessment of how you, our applicant frontend developer, would tackle the creation of a frontend view. Don't feel obligated to spend too much time, an hour or two should be more than enough, and we are grateful for your efforts. 

### The Requirements
Take a look at the web page provided [index.html](index.html). There are 3 main sections to this page; a restaurant/cafe profile, review panel, and a review creation panel. 

- Use your skills as a frontend developer, style the page to be as user friendly as possible. 
- Using the [API provided](https://dps-fe-test-api.herokuapp.com/0), load the data dynamically. The API will return restaurant information (increment the index).
- Make the page dynamic and responsive to input. 

###### API Responses
* Attica Index = https://dps-fe-test-api.herokuapp.com/0
* The French Laundry Index = https://dps-fe-test-api.herokuapp.com/1
* Blue Hill at Stone Barns Index = https://dps-fe-test-api.herokuapp.com/2
* Random result - https://dps-fe-test-api.herokuapp.com/rand

### The rules
There are no rules. We want you to show us your skills in the way you want. Use whatever technologies you are disposed to use. Bootstrap has been included by default, but can be removed and replaced with whatever you may prefer. 

To be perfectly explicit, you have complete creative control. Play into your strengths, and show us what you do best. 

When you are ready to show us, feel free to send it through via your preferred method; but we encourage you to submit a public github repository for us to review. 

### Good luck!
If you have any questions, concerns, or issues, feel free to contact the creator of this challenge via email - [Ari Molzer - ari.molzer@dps.com.au](mailto:ari.molzer@dps.com.au). 